<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeople extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('people', function (Blueprint $table) {
            $table->integer('id_user');
            $table->string('name');
            $table->date('birthdate');
            $table->string('phone');
            $table->string('email');
            $table->string('cpf');
            $table->string('address');
            $table->integer('address_number');
            $table->string('address_complement');
            $table->string('address_state');
            $table->string('address_city');
            $table->string('address_code');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('people');
    }
}
