<?php

namespace Mgzaspuc\Clients;

use Illuminate\Database\Eloquent\Model;

class People extends Model
{
    protected $table = 'people';
}
