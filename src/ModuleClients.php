<?php

namespace Mgzaspuc\Clients;

use Illuminate\Support\ServiceProvider;

class ModuleClients extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/migrations' => base_path('database/migrations/')
        ]);
    }
}